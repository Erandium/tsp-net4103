


##2-a

import matplotlib.pylab as plt
import networkx as nx

def plot_degreeDistr(G, c) :
    degrees = [G.degree(n) for n in G.nodes()]
    plt.hist(degrees, len(degrees) // 5, color = c)

GrCaltch = nx.read_graphml("fb100/Caltech36.graphml")
GrMIT = nx.read_graphml("fb100/MIT8.graphml")
GrJohnsHopkins = nx.read_graphml("fb100/Johns Hopkins55.graphml")

plot_degreeDistr(GrCaltch, "blue")
plt.figure()


plot_degreeDistr(GrMIT, "red")
plt.figure()


plot_degreeDistr(GrJohnsHopkins, "green")

plt.show()

##2-b

import networkx as nx

GrCaltch = nx.read_graphml("fb100/Caltech36.graphml")
GrMIT = nx.read_graphml("fb100/MIT8.graphml")
GrJohnsHopkins = nx.read_graphml("fb100/Johns Hopkins55.graphml")

print("réseau Caltch")
print("coefficient de clustering global : " + str(nx.transitivity(GrCaltch)))
print("coefficient de clustering moyen : " + str(nx.average_clustering(GrCaltch)))
print("densité des connections : " + str(nx.density(GrCaltch)))

print("")

print("réseau MIT")
print("coefficient de clustering global : " + str(nx.transitivity(GrMIT)))
print("coefficient de clustering moyen : " + str(nx.average_clustering(GrMIT)))
print("densité des connections : " + str(nx.density(GrMIT)))

print("")

print("réseau Johns Hopkins")
print("coefficient de clustering global : " + str(nx.transitivity(GrJohnsHopkins)))
print("coefficient de clustering moyen : " + str(nx.average_clustering(GrJohnsHopkins)))
print("densité des connections : " + str(nx.density(GrJohnsHopkins)))

##2-c

import matplotlib.pylab as plt
import networkx as nx

def plot_degree_clustering(G, c) :
    degrees = [G.degree(n) for n in G.nodes()]
    clusteringDict = nx.clustering(G)
    clusterings = [clusteringDict.get(n) for n in G.nodes()]
    plt.scatter(degrees,clusterings, c)
    plt.xlabel("degré")
    plt.ylabel("clustering local")


GrCaltch = nx.read_graphml("fb100/Caltech36.graphml")
GrMIT = nx.read_graphml("fb100/MIT8.graphml")
GrJohnsHopkins = nx.read_graphml("fb100/Johns Hopkins55.graphml")

plot_degree_clustering(GrCaltch, "ob")
plt.figure()

plot_degree_clustering(GrMIT, "or")
plt.figure()

plot_degree_clustering(GrJohnsHopkins, "og")
plt.show()


##3

import csv
import matplotlib.pylab as plt

networkSizes = []
studentAssort = []
majorAssort = []
vertexAssort = []
dormAssort = []
densities = []

with open("donnees15.csv", "r", newline='') as csvData:
    reader = csv.DictReader(csvData)
    for row in reader :
        networkSizes.append(int(row['Network Size']))
        studentAssort.append(float(row['Student Status Assortativity']))
        majorAssort.append(float(row['Major Assortativity']))
        vertexAssort.append(float(row['Vertex Degree Assortativity']))
        dormAssort.append(float(row['Dorm Assortativity']))
        densities.append(float(row['densite']))




plt.scatter(networkSizes, studentAssort, color="blue")
plt.xscale('log')
plt.xlabel("taille")
plt.ylabel("assortativité étudiante")
plt.figure()
plt.hist(studentAssort, len(studentAssort) * 5, color = "blue")
plt.xlabel("assortativité étudiante")
plt.figure()

plt.scatter(networkSizes, majorAssort, color="red")
plt.xscale('log')
plt.xlabel("taille")
plt.ylabel("assortativité majeur")
plt.figure()
plt.hist(majorAssort, len(majorAssort) * 5, color = "red")
plt.xlabel("assortativité majeur")
plt.figure()

plt.scatter(networkSizes, vertexAssort, color="green")
plt.xscale('log')
plt.xlabel("taille")
plt.ylabel("assortativité sommets")
plt.figure()
plt.hist(vertexAssort, len(vertexAssort) * 5, color = "green")
plt.xlabel("assortativité sommets")
plt.figure()

plt.scatter(networkSizes, dormAssort, color="orange")
plt.xscale('log')
plt.xlabel("taille")
plt.ylabel("assortativité internat")
plt.figure()
plt.hist(dormAssort, len(dormAssort) * 5, color = "orange")
plt.xlabel("assortativité internat")

plt.show()



##4

import matplotlib.pylab as plt
import random
import networkx as nx

def listCommonNeighbors(G, x, y):
    res = []
    neighborsX = g.neighbors(x)
    neighborsY = g.neighbors(y)
    for nodeX in neighboreX:
        for nodeY in neighboreY:
            if nodeX == nodeY:
                res.append(nodeX)
    return res


def nbCommonNeighbors(G, x, y):
    return len(listCommonNeighbors(G, x, y))

def jaccardCoeff(G, x, y):
    nbInter = nbCommonNeighbors(G, x, y)
    nbUnion = g.degree(x) + g.degree(y) - nbInter
    return nbInter/nbUnion

def adamicAdar(G, x, y):
    res = 0
    for node in listCommonNeighbors(G, x, y):
        res += 1/log(len(g.neighbors(node)))
    return res




def removalFreq(G, f):
    nbDeletedEdges = int(f*G.number_of_edges())
    Gprim = G.copy()
    listEdges = list(G.edges())
    removedEdges = []

    for i in range(nbDeletedEdges):
        r = random.randint(0, len(listEdges)-1)
        u, v = listEdges[r]
        Gprim.remove_edge(u,v)
        removedEdges.append((u,v))
        listEdges.pop(r)

    return Gprim, removedEdges

'''
not working

def bestScoreCommonNeighbors(G, n) :
    listScore = [0 for i in range(n)]
    listNodes = [(0,0) for i in range(n)]
    nbNodes = len(G.nodes())
    for i in range(nbNodes):
        for j in range(i+1, nbNodes):
            x = G.nodes()[i]
            y = G.nodes()[j]
            score = nbCommonNeighbors(G, x, y)
            nodes = (x,y)
            for k in range(n):
                if score > list[k] :
                    tmpScore = listScore[k]
                    listScore[k] = score
                    score = tmpScore
                    tmpNodes = listNodes[k]
                    listNodes[k] = nodes
                    nodes = tmpNodes
    return listNodes, listScore
'''


GrCaltch = nx.read_graphml("fb100/Caltech36.graphml")

Gprim, removedEdges = removalFreq(GrCaltch, 0.05)






























